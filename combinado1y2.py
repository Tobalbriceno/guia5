def openfile():
    files = open("co2_emission.csv")
    totalpaises = []
    paises = []
    pais = ""
    for linea in files:
        pais = linea.split(",")[0]
        totalpaises.append(pais)
        if pais not in paises:
            paises.append(pais)
    files.close()
    return paises, totalpaises 

def cantregister(paises, totalpaises):
    cantidad = []
    for pais in paises:
        register = 0
        for myne in totalpaises:
            if pais == myne:
                register = register + 1
        cantidad.append(register)
    maximo = max(cantidad)
    stoked = cantidad.index(maximo)
    return cantidad, stoked

def openfile1():
    filecarpet = open("co2_emission.csv")
    paisestotales = []
    paises = []
    pais = ""
    for linea in filecarpet:
        pais = linea.split(",")[0]
        paisestotales.append(pais)
        if pais not in paises:
            paises.append(pais)
    filecarpet.close()
    paises.remove("Entity")
    return paises, paisestotales     

def count_register(paises, paisestotales):
    cantidad1 = []
    for pais in paises:
        regis = 0
        for myne in paisestotales:
            if pais == myne:
                regis = regis + 1
        cantidad1.append(regis)
    minimo = min(cantidad1)
    stoked1 = cantidad1.index(minimo)
    return cantidad1, stoked1

paises, totalpaises = openfile()
cantidad, stoked = cantregister(paises, totalpaises)
paises, paisestotales = openfile1()
cantidad1, stoked1 = count_register(paises, paisestotales)
print("Los paises con mayor y menor cantidad de muestras son: ")
print("Mayor cantidad de muestras: \n",paises[stoked])
print("Cantidad de muestras:", cantidad[stoked])    
print("Menor cantidad de muestras: \n",paises[stoked1])
print("Cantidad de muestras de CO2:", cantidad1[stoked1])
