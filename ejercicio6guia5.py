import os
def file_open():
    files = open("co2_emission.csv")
    diox= []
    count = 0
    tone = ""
    for linea in files:
        count = count + 1
        tone = linea.split(",")[3]
        diox.append(tone)
    files.close()
    count = count -1
    diox.pop(0) #elimina un elemento por su indice
    return diox, count

#funcion para convertir los valores en decimales
def converse_dat(diox):
    toneladas1 = []
    for element in diox:
        toneladas1.append(float(element))
    return toneladas1

#funcion para sacar el promedio
def media_pais(toneladas1, count):
    pluys = 0
    for element in toneladas1:
        pluys = pluys + element
    media = pluys/count
    print("la media es: ",media,"toneladas")
    margen = media*0.1
    margen1 = media - margen
    margen2 = media + margen 
    print("el margen de error es de un 10%, por lo tanto se expresa en [",margen1,",",margen2,"]")
    



#llamo a la funcion principal
diox, count = file_open()
toneladas1 = converse_dat(diox)
media_pais(toneladas1, count)